import { useState } from "react";
import Component1 from "./components/Component1";
import "./App.css";

function App() {
  const [data, setData] = useState({
    data1: "François",
    data2: "Jessica",
    data3: "Karim",
    data4: "Natasha",
  });

  return (
    <>
      <h1>Exercice Exo React Props</h1>
      <div className="card">
        <Component1 />
      </div>
      <p className="description">Passage de props entre composants</p>
    </>
  );
}

export default App;
