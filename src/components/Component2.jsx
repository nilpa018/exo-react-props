import React from "react";
import Component3 from "./Component3";

const Component2 = () => {
  return (
    <div className="display-data">
      <div className="component-container">
        <p className="component-title">Component 2</p>
        <p className="component-data">{/* {Data2} */}</p>
      </div>
      <Component3 />
    </div>
  );
};

export default Component2;
