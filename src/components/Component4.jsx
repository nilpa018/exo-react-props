import React from "react";

const Component4 = () => {
  return (
    <div className="display-data">
      <div className="component-container">
        <p className="component-title">Component 4</p>
        <p className="component-data">
          {/* {Data1} & {Data2} & {Data3} & {Data4} */}
        </p>
      </div>
    </div>
  );
};

export default Component4;
