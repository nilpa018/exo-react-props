import React from "react";
import Component2 from "./Component2";

const Component1 = () => {
  return (
    <div className="display-data">
      <div className="component-container">
        <p className="component-title">Component 1</p>
        <p className="component-data">{/* {Data1} & {Data2} */}</p>
      </div>
      <Component2 />
    </div>
  );
};

export default Component1;
