import React from "react";
import Component4 from "./Component4";

const Component3 = () => {
  return (
    <div className="display-data">
      <div className="component-container">
        <p className="component-title">Component 3</p>
        <p className="component-data">{/* {Data3} */}</p>
      </div>
      <Component4 />
    </div>
  );
};

export default Component3;
