# Exercice React : Passage de props entre composants

## Objectif

Vous disposez de 4 composants s'imbriquant les uns dans les autres qui doivent se transmettrent des props entre eux.  
Cet exercice vous permettra de travailler sur le passage de props ainsi que sur la difficulté à passer des informations à un composant qui est profondément inclus dans d'autres composants.

## Prérequis

Assurez-vous d'avoir Node.js installé sur votre machine.

## Étapes

### Étape 1: Initialisation du projet React

1. Clonez le repository depuis GitLab :

   ```bash
   git clone https://gitlab.com/nilpa018/exo-react-props.git
   ```

2. Accédez au répertoire du projet :

   ```bash
   cd exo-react-props
   ```

3. Lancez l'installation des modules et construction du dossier public :

   ```bash
   npm install
   ```

4. Lancez l'application pour vous assurer que tout fonctionne correctement :
   ```bash
   npm run dev
   ```

### Étape 2: Passage des informations

Dans le dossier src/components vous retrouverez les composants nécessaires à utiliser:

**Component1**: Doit recevoir et afficher data1 et data2 provenant de App.jsx.  
**Component2**: Doit uniquement recevoir et afficher data2 provenant de Component1.jsx.  
**Component3**: Doit uniquement recevoir et afficher data3 provenant de App.jsx.  
**Component4**: Doit recevoir le state data directement depuis App.jsx sans passer par les autres composants.

Dans les composants, le lieu d'affichage des données (data) est déjà prêt, vous avez juste à décommenter.

### Étape 3: Conditions de réussite

Les composants 1 et 2 recoivent leurs props depuis le composant.  
Les composants 3 et 4 recoivent leurs props sans utiliser le Prop Drilling. (Plusieurs solutions possibles)

Assurez-vous d'inclure le code source complet pour la vérification.

### Étape 4: Vérification

Le candidat devra fournir l'ensemble des fichiers à l'exception des node_modules après avoir complété la tâche.

### Étape 5: Styles (bonus)

Le style ne doit pas être modifié.

### Aperçu graphique

Voici une représentation de ce qui est attendu graphiquement, vous avez la possibilité de modifier un peu celui-ci.

![Representation graphique de l'exercice](/images/screen-exo-react-props.png)  
![Representation visuelle attendue](/images/exo-react-props.png)  

### Remarques

- Afin de répondre aux meilleurs pratiques, déclarez vos variables de préférences avec const et let.
- Vous avez le droit d'ajouter des fonctionnalités au projet (loader, tests, TypeScript etc...)
- Utilisez les composants de manière modulaire et réutilisable.
- Commentez votre code pour expliquer votre démarche si c'est pertinent.

### Ressource

Documentation React : https://reactjs.org/docs/getting-started.html

### Soumission

Lorsque vous avez terminé, assurez-vous de fournir le code source du projet pour évaluation soit en l'envoyant au format ZIP sans y inclure les node_modules par [E-mail](mailto:nilpa018@yahoo.fr) soit en l'hébergeant sur un Drive afin de pouvoir télécharger celui-ci.
